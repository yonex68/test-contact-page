import React from 'react';

import Header from '../header';

const Layout = ({ children }) => (
  <>
    <Header />
    <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
      {children}
    </div>
  </>
);

export default Layout;