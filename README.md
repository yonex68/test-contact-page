# Test technique

Ce projet a été créé avec [Create React App](https://github.com/facebook/create-react-app).

## Pré-requis

- `node` >=v14.18.0
- `npm` >=7.14.0

## Installation

Clonez et installez `test-contact-page` en effectuant les commandes suivantes:

```bash
# Clone le projet dans votre dossier local
git clone https://gitlab.com/AxelPeter/test-contact-page.git

# Accède au dossier du projet
cd test-contact-page

# Installe les dépendances du projet
npm install
```

## Développement

### Démarrage de l'application

Pour démarrer l'application en mode `développement`, lancez la commande suivante:

```bash
npm start
```
